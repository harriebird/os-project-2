$(document).ready(function () {
    let procCount = 0;
    let processes = [];

    $('#new-proc').click(function () {
        if(isNaN($('#b-time').val()) || $('#b-time').val() =='')
            $('#invalid-modal').modal('show');
        else {
            procCount++;
            $('#preview-table > tbody:last-child').append(`<tr><th scope="row">${procCount}</th><td>${$('#b-time').val()}</td></tr>`);
            processes.push({process: procCount, burst_time: Number($('#b-time').val()) })
            $('#b-time').val('');
        }
    })

    $('#srtf-new-proc').click(function () {
        if(isNaN($('#b-time').val()) || $('#b-time').val() =='' || isNaN($('#a-time').val()) || $('#a-time').val() =='')
            $('#invalid-modal').modal('show');
        else {
            procCount++;
            $('#preview-table > tbody:last-child').append(`<tr><th scope="row">${procCount}</th><td>${$('#a-time').val()}</td><td>${$('#b-time').val()}</td></tr>`);
            processes.push({process: procCount, arrival_time: Number($('#a-time').val()), burst_time: Number($('#b-time').val()) })
            $('#a-time').val('');
            $('#b-time').val('');
        }
    })

    $('#priority-new-proc').click(function () {
        if(isNaN($('#b-time').val()) || $('#b-time').val() =='' || isNaN($('#priority').val()) || $('#priority').val() =='')
            $('#invalid-modal').modal('show');
        else {
            procCount++;
            $('#preview-table > tbody:last-child').append(`<tr><th scope="row">${procCount}</th><td>${$('#priority').val()}</td><td>${$('#b-time').val()}</td></tr>`);
            processes.push({process: procCount, priority: Number($('#priority').val()), burst_time: Number($('#b-time').val()) })
            $('#priority').val('');
            $('#b-time').val('');
        }
    })


    $('#submit-inputs').click(function () {
        if (procCount < 1)
            $('#invalid-modal').modal('show');
        else {
            $('#process-list').val(JSON.stringify(processes));
            $('#proc-form').submit();
        }
    })
});

