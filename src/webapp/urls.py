from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('technique/<str:technique>/', views.use_technique, name='use_technique')
]
