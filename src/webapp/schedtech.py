from plotly.offline import plot
import plotly.figure_factory as ff
from operator import itemgetter
import json


def compute_awt(waiting_times, processes):
    return sum(waiting_times[0:int(len(waiting_times)-1)])/len(processes)


def sort_processes(processes, key):
    return sorted(processes, key=itemgetter(key))


def sum_process_value(processes, key):
    return sum(item[key] for item in processes)


def create_graph(data):
    fig = ff.create_gantt(data, group_tasks=True, index_col='Resource', reverse_colors=True, height=300, show_colorbar=True)
    fig['layout']['xaxis'].update({'type': None})
    plt_div = plot(fig, output_type='div')
    return plt_div


def use_fcfs(request):
    points = [0]
    chart_data = []
    processes = json.loads(request.POST.get('process-list', ''))
    for process in processes:
        points.append(points[-1] + process['burst_time'])
        chart_data.append(dict(Task="Process", Start=points[-2],
                               Finish=points[-1], Resource='Process {}'.format(process['process'])))

    chart = create_graph(chart_data)
    awt = compute_awt(points, processes)
    return {'chart': chart, 'values': processes, 'points': points, 'awt': awt}


def use_sjf(request):
    points = [0]
    chart_data = []
    processes = json.loads(request.POST.get('process-list', ''))
    sorted_processes = sort_processes(processes, 'burst_time')
    for process in sorted_processes:
        points.append(points[-1] + process['burst_time'])
        chart_data.append(dict(Task="Process", Start=points[-2],
                               Finish=points[-1], Resource='Process {}'.format(process['process'])))

    chart = create_graph(chart_data)
    awt = compute_awt(points, processes)
    return {'chart': chart, 'values': processes, 'points': points, 'awt': awt}


def use_srtf(request):
    points = [0]
    chart_data = []
    processes = json.loads(request.POST.get('process-list', ''))
    sorted_processes = sort_processes(processes, 'arrival_time')


def use_rr(request):
    points = [0]
    chart_data = []
    processes = json.loads(request.POST.get('process-list', ''))
    quantumn_time = int(request.POST.get('q-time', ''))
    mod_processes = processes.copy()
    running_time = sum_process_value(mod_processes, 'burst_time')

    while running_time > 0:
        for index, process in enumerate(mod_processes):
            if process['burst_time'] < 1:
                del mod_processes[index]
            else:
                process['burst_time'] -= quantumn_time
                mod_processes[index] = process
                if process['burst_time'] > quantumn_time:
                    points.append(points[-1] + quantumn_time)
                    running_time -= quantumn_time
                else:
                    points.append(points[-1] + process['burst_time'])
                    running_time -= process['burst_time']

            chart_data.append(dict(Task="Process", Start=points[-2],
                                   Finish=points[-1], Resource='Process {}'.format(process['process'])))

    chart = create_graph(chart_data)
    awt = compute_awt(points, processes)
    return {'chart': chart, 'values': processes, 'points': points, 'awt': awt}


def use_priority(request):
    points = [0]
    chart_data = []
    processes = json.loads(request.POST.get('process-list', ''))
    sorted_processes = sort_processes(processes, 'priority')
    for process in sorted_processes:
        points.append(points[-1] + process['burst_time'])
        start = points[-2]
        end = points[-1]
        chart_data.append(dict(Task="Process", Start=start,
                               Finish=end, Resource='Process {}'.format(process['process'])))

    chart = create_graph(chart_data)
    awt = compute_awt(points, processes)
    return {'chart': chart, 'values': processes, 'points': points, 'awt': awt}


def use_priority_rr():
    pass


