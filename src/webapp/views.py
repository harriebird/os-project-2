from django.shortcuts import render
from .schedtech import use_fcfs, use_sjf, use_rr, use_srtf, use_priority


def index(request):
    return render(request, 'webapp/index.html')


def use_technique(request, technique):
    if technique == 'fcfs':
        if request.method == 'POST':
            result = use_fcfs(request)
            return render(request, 'webapp/technique.html', {'data': {'technique': technique, 'result': result}})
        else:
            return render(request, 'webapp/technique.html', {'data': {'technique': technique}})
    elif technique == 'sjf':
        if request.method == 'POST':
            result = use_sjf(request)
            return render(request, 'webapp/technique.html', {'data': {'technique': technique, 'result': result}})
        else:
            return render(request, 'webapp/technique.html', {'data': {'technique': technique}})
    elif technique == 'srtf':
        if request.method == 'POST':
            result = use_srtf(request)
            return render(request, 'webapp/technique.html', {'data': {'technique': technique, 'result': result}})
        else:
            return render(request, 'webapp/technique.html', {'data': {'technique': technique}})
    elif technique == 'rr':
        if request.method == 'POST':
            result = use_rr(request)
            return render(request, 'webapp/technique.html', {'data': {'technique': technique, 'result': result}})
        else:
            return render(request, 'webapp/technique.html', {'data': {'technique': technique}})
    elif technique == 'priority':
        if request.method == 'POST':
            result = use_priority(request)
            return render(request, 'webapp/technique.html', {'data': {'technique': technique, 'result': result}})
        else:
            return render(request, 'webapp/technique.html', {'data': {'technique': technique}})
    elif technique == 'priority-rr':
        if request.method == 'POST':
            result = use_srtf(request)
            return render(request, 'webapp/technique.html', {'data': {'technique': technique, 'result': result}})
        else:
            return render(request, 'webapp/technique.html', {'data': {'technique': technique}})
    else:
        pass

    # plt_div = graph()
    return render(request, 'webapp/technique.html', {'data': {'chart': plt_div, 'technique': technique}})
